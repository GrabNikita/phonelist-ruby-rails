class CreateCharacteristicValues < ActiveRecord::Migration[5.1]
  def change
    create_table :characteristic_values do |t|
      t.references :phone, foreign_key: true
      t.references :characteristic_type, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
