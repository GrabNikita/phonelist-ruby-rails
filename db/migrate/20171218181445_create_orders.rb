class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :phone_number
      t.references :phone, foreign_key: true

      t.timestamps
    end
  end
end
