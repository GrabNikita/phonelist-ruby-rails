class AddImageToPhones < ActiveRecord::Migration[5.1]
  def change
    add_reference :phones, :image, foreign_key: true
  end
end
