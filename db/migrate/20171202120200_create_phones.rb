class CreatePhones < ActiveRecord::Migration[5.1]
  def change
    create_table :phones do |t|
      t.string :name
      t.decimal :price
      t.text :description
      t.integer :rating
      t.integer :sort
      t.date :realese_date
      t.references :model, foreign_key: true

      t.timestamps
    end
  end
end
