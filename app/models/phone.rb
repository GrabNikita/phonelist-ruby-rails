class Phone < ApplicationRecord
  belongs_to :model
  validates :name, :price, presence: true
  has_many :characteristic_values, dependent: :destroy
  has_many :orders
end
