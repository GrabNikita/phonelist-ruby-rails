class CharacteristicValue < ApplicationRecord
    belongs_to :phone
    belongs_to :characteristic_type
    validates :value, presence: true
end
