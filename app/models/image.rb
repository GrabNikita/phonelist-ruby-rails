class Image < ApplicationRecord
    has_one :phone
    validate :check_image
    
    def initialize(params = {})
      @file = params.delete(:file)
      super
      if @file
        self.filename = sanitize_filename(@file.original_filename)
        self.content_type = @file.content_type
        self.file_contents = @file.read
      end
    end
    
    private
      def sanitize_filename(filename)
        return File.basename(filename)
      end
      
      def check_image
        if !["image/jpeg", "image/pjpeg", "image/png", "image/x-png", "image/gif"].include?(@file.content_type)
          errors.add(:file, 'File must be image.')
        end
      end
end
