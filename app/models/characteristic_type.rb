class CharacteristicType < ApplicationRecord
    validates :name, presence: true
    has_many :characteristic_values, dependent: :destroy
end
