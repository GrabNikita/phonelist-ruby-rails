class Model < ApplicationRecord
  belongs_to :brand
  has_many :phone
  validates :name, presence: true
end
