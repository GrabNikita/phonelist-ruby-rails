class BrandsController < ApplicationController
    
   before_action :authenticate, :except => [:index, :show]
    
    def index
       
       @pageTitle = "Бренды"
       @brands = Brand.all
    end
    
    def show
        
        @brand = Brand.find(params[:id])
        @pageTitle = @brand.name
    end
    
    def new
       
       @pageTitle = "Добавить бренд"
       @brand = Brand.new
    end
    
    def edit
        
        @brand = Brand.find(params[:id])
        @pageTitle = @brand.name
    end
    
    def create
        
        @brand = Brand.new(brand_params)
        
        if @brand.save
            @pageTitle = @brand.name
            redirect_to @brand
        else
            @pageTitle = "Добавить бренд"
            render new_brand_path
        end
    end
    
    def update
       
       @brand = Brand.find(params[:id])
       if @brand.update(brand_params)
           @pageTitle = @brand.name
            redirect_to @brand
       else 
            render edit_brand_path(@brand)
       end
    end
    
    def destroy
        
      @brand = Brand.find(params[:id])
      @brand.destroy
     
      redirect_to brands_path
    end
    
    private
    
        def brand_params
           params.require(:brand).permit(:name, :description)
        end
end
