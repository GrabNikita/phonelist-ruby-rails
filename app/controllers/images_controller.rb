class ImagesController < ApplicationController
  before_action :authenticate, :except => [:show]
  def index
    @images = Image.all
  end
  
  def show
    
    @image = Image.find(params[:id])
    
    send_data(@image.file_contents,
            type: @image.content_type,
            filename: @image.filename,
            disposition: 'inline'
            )
  end
  
  def destroy
    @image = Image.find(params[:id])
    @image.destroy
    
    redirect_to images_path
  end
  
  private
    def document_params
      params.require(:document).permit(:file)
    end
end
