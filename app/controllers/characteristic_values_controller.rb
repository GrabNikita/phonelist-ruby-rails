class CharacteristicValuesController < ApplicationController
    
    before_action :authenticate
    
    def create
        
        @phone = Phone.find(params[:phone_id])
        @charValue = @phone.characteristic_values.create(characteristic_value_params)
        
        redirect_to phone_path(@phone)
    end
    
    def update
    
        @phone = Phone.find(params[:phone_id])
        @charValue = @phone.characteristic_values.find(params[:id])
        
        @charValue.update(characteristic_value_params)
        
        redirect_to phone_path(@phone)
    end
    
    def destroy
        
        @phone = Phone.find(params[:phone_id])
        @charValue = @phone.characteristic_values.find(params[:id])
        
        @charValue.destroy
        
        redirect_to phone_path(@phone)
    end
    
    private
        def characteristic_value_params
           
           params.require(:characteristic_value).permit(:value,:phone_id,:characteristic_type_id) 
        end
end
