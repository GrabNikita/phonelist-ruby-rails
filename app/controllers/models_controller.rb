class ModelsController < ApplicationController
    
    before_action :authenticate
     
    def create
        
        @brand = Brand.find(params[:brand_id])
        
        @model = @brand.models.create(model_params)
        
        redirect_to brand_path(@brand)
    end
    
    def update
    
        @brand = Brand.find(params[:brand_id])
        @model = @brand.models.find(params[:id])
        
        @model.update(model_params)
        
        redirect_to brand_path(@brand)
    end
    
    def destroy
        
        @brand = Brand.find(params[:brand_id])
        @model = @brand.models.find(params[:id])
        
        @model.destroy
        
        redirect_to brand_path(@brand)
    end
    
    private
        def model_params
           
           params.require(:model).permit(:name) 
        end
end
