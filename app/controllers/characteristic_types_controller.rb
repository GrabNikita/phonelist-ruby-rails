class CharacteristicTypesController < ApplicationController
    
    before_action :authenticate, :except => [:index]
    
    def index
       
       @pageTitle = "Типы характеристик"
       @charTypes = CharacteristicType.all
    end
    
    def show
        @charType = CharacteristicType.find(params[:id])
        @pageTitle = @charType.name
    end
    
    def new
        
        @pageTitle = "Добавить тип характеристики"
        @charType = CharacteristicType.new
        
    end
    
    def edit 
        
        @charType = CharacteristicType.find(params[:id])
        @pageTitle = @charType.name
    end
    
    def create
        
        @charType = CharacteristicType.new(characteristic_type_params)
        
        if @charType.save
            @pageTitle = @charType.name
            redirect_to characteristic_types_path
        else
            @pageTitle = "Добавить тип"
            render new_characteristic_type_path
        end
    end
    
    def update
       
       @charType = CharacteristicType.find(params[:id])
       if @charType.update(characteristic_type_params)
           @pageTitle = @charType.name
            redirect_to @charType
       else 
            render edit_characteristic_type_path(@brand)
       end
    end
    
    def destroy
        
      @charType = CharacteristicType.find(params[:id])
      @charType.destroy
     
      redirect_to characteristic_types_path
    end
    
    
    private
    
        def characteristic_type_params
           params.require(:characteristic_type).permit(:name)
        end
end
