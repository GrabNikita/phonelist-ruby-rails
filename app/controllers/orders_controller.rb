class OrdersController < ApplicationController
    
    before_action :authenticate, :except => [:create]
    
    def index
        
        @pageTitle = "Заказы"
        @orders = Order.order(:created_at => "DESC").all
    end
    
    def edit
        
        @order = Order.find(params[:id])
        @pageTitle = @order.name + " - " + @order.phone_id.to_s
    end
    
    def create
        
        @phone = Phone.find(order_params[:phone_id])
        
        if @phone.nil?
            redirect_to phones_path
        else
            @order = Order.new(order_params)
            if(@order.save)
                redirect_to phone_path(@phone)
            else
                render phone_path(@phone)
            end
        end
    end
    
    def update
        @order = Order.find(params[:id])
       if @order.update(order_params)
            redirect_to orders_path
       else 
            render edit_order_path(@order)
       end
    end
    
    def destroy
        
        @order = Order.find(params[:id])
        @order.destroy
        
        redirect_to phones_path
    end
    
    private 
        def order_params
           params.require(:order).permit(:name, :phone_number, :phone_id) 
        end
end
