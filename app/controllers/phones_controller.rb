class PhonesController < ApplicationController
    
    before_action :authenticate, :except => [:index, :show]
    
    def index
       
        @pageTitle = "Список телефонов"
        @phones = Phone.all.model.all
        @order = Order.new
    end
    
    def show
        
        @phone = Phone.find(params[:id])
        @pageTitle = @phone.name
        @charValues = @phone.characteristic_values.all
        @order = Order.new
    end
    
    def new
       
       @pageTitle = "Добавить телефон"
       @phone = Phone.new
       
       @brands = Brand.all
       @models = Model.all
    end
    
    def edit
        
        @phone = Phone.find(params[:id])
        @pageTitle = @phone.name
    end
    
    def create
        
        @image = Image.new(image_params)
        
        if @image.save
            
            @phone = Phone.new(phone_params)
            
            @phone.image_id = @image.id
            
            if @phone.save
                redirect_to @phone
            else
                @pageTitle = "Добавить телефон"
                render new_phone_path
            end      
        else
            @pageTitle = "Добавить телефон"
            render new_phone_path
        end
    end
    
    def update
       
        @phone = Phone.find(params[:id])
        
        if !image_params.blank?
            
            @phone.image.filename = params[:phone][:file].original_filename
            @phone.image.content_type = params[:phone][:file].content_type
            @phone.image.file_contents = params[:phone][:file].read
            
            @phone.image.update_attributes(image_params)
        end
        
        if @phone.update(phone_params)
            @pageTitle = @phone.name
            redirect_to @phone
        else 
            render edit_phone_path(@phone)
        end
    end
    
    def destroy
        
        @phone = Phone.find(params[:id])
        if !@phone.image_id.nil?
            @image = Image.find(@phone.image_id)
            if !@image.nil?
                @phone.image_id = nil
                @phone.image_id = nil
                @phone.update_attributes({:image_id => nil})
                @image.destroy
            end
        end
        @phone.destroy
     
        redirect_to phones_path
    end
    
    private
    
        def phone_params
           params.require(:phone).permit(:id, :name, :description, :model_id, :price, :sort, :rating, :date_realese, :image_id)
        end
        
        def image_params
            params.require(:phone).permit(:file)
        end
end
