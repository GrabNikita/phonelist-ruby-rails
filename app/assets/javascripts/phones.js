// JavaScript File
(function($){
    
    $(document).ready(function() {
        
         $(".characteristics-wrapper").on("click", ".show-edit-form",function (event) {
            
            var $this = $(this);
            var $charWrapper = $this.closest(".characteristic-wrapper");
            if($charWrapper.length <= 0) {
                return;
            }
            
            var $formWrapper = $charWrapper.find(".form-wrapper");
            if($formWrapper.length <= 0) {
                return;
            }
            
            $formWrapper.slideToggle();
        });
        
        $(".show-characteristics").click(function(){
           
           var $this = $(this);
           
           var $phoneWrapper = $this.closest(".phone-wrapper");
           
           if($phoneWrapper.length <= 0) {
               return;
           }
           
           var $charsWrapper = $phoneWrapper.find(".characteristics-wrapper");
           if($charsWrapper.length <= 0) {
               return;
           }
           
           $charsWrapper.slideToggle();
        });
        
        $(".order-form-btn").click(function(){
           
           var $this = $(this);
           var phoneId = $this.data("phone-id");
           
           var $phoneIdInput = $("#order-form input#phone-id");
           $phoneIdInput.val(phoneId);
           
           $("#order-form").modal();
        });
    })
})(jQuery);