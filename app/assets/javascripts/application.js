// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
(function($){
    
    $(document).ready(function() {
        
        $(".list-group").on("click", ".show-edit-form",function (event) {
            
            var $this = $(this);
            console.log(this)
            var $listItem = $this.closest(".list-group-item");
            if($listItem.length <= 0) {
                return;
            }
            
            var $formWrapper = $listItem.find(".form-wrapper");
            if($formWrapper.length <= 0) {
                return;
            }
            
            $formWrapper.slideToggle();
        });
        
        $('*[mask="phone"]').inputmask("9(999) 999-99-99");
    });
})(jQuery);