Rails.application.routes.draw do
    
  get 'images/show'

    resources :brands do
       resources :models 
    end
    
    resources :characteristic_types
    
    resources :phones do
        resources :characteristic_values
    end
    
    resources :images
    
    resources :orders
    
    root 'phones#index'
    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
